package isr.validator;

import isr.exception.DateFormatException;

/**
 * Validator for the Login Service
 */
public final class LoginValidator
{

	/**
	 * Validates the date if size is 8 and numeric
	 * @param date The date
	 * @throws DateFormatException The date is invalid
	 */
	public static void validateDate(String field, String value) throws DateFormatException
	{
		if (value != null)
		{
			// Check if size is 8
			if (8 != value.length())
			{
				throw new DateFormatException("The date format " + value + " for " + field + " is invalid. Should be length of 8.");
			}

			// Check if numeric
			try
			{
				Integer.parseInt(value);
			}
			catch (NumberFormatException e)
			{
				throw new DateFormatException("The date format " + value + " for " + field + " is invalid. Should be numeric.");
			}
		}
	}
}
