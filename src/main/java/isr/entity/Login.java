package isr.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Login entity
 */
public class Login
{
	@Field("login_time")
	private Date loginTime;

	@Id
	public String id;
	private String user;
	private String attribute1;
	private String attribute2;
	private String attribute3;
	private String attribute4;

	/**
	 * Retrieves the login time
	 * @return login time
	 */
	public Date getLoginTime()
	{
		return loginTime;
	}

	/**
	 * Sets the login time
	 * @param loginTime The login time to set
	 */
	public void setLoginTime(Date loginTime)
	{
		this.loginTime = loginTime;
	}

	/**
	 * Retrieves the user
	 * @return The user
	 */
	public String getUser()
	{
		return user;
	}

	/**
	 * Sets the user
	 * @param user The user to set
	 */
	public void setUser(String user)
	{
		this.user = user;
	}

	/**
	 * Retrieves the attribute1
	 * @return The attribute1
	 */
	public String getAttribute1()
	{
		return attribute1;
	}

	/**
	 * Sets the attribute1
	 * @param attribute1 The attribute1 to set
	 */
	public void setAttribute1(String attribute1)
	{
		this.attribute1 = attribute1;
	}

	/**
	 * Retrieves the attribute2
	 * @return The attribute2
	 */
	public String getAttribute2()
	{
		return attribute2;
	}

	/**
	 * Sets the attribute2
	 * @param attribute2 The attribute2 to set
	 */
	public void setAttribute2(String attribute2)
	{
		this.attribute2 = attribute2;
	}

	/**
	 * Retrieves the attribute3
	 * @return The attribute3
	 */
	public String getAttribute3()
	{
		return attribute3;
	}

	/**
	 * Sets the attribute3
	 * @param attribute3 The attribute3 to set
	 */
	public void setAttribute3(String attribute3)
	{
		this.attribute3 = attribute3;
	}

	/**
	 * Retrieves the attribute4
	 * @return The attribute4
	 */
	public String getAttribute4()
	{
		return attribute4;
	}

	/**
	 * Sets the attribute4
	 * @param attribute4 The attribute4 to set
	 */
	public void setAttribute4(String attribute4)
	{
		this.attribute4 = attribute4;
	}
}
