package isr.entity;

/**
 * Login Count entity
 */
public class LoginCount
{
	private String user;
	private long count;

	/**
	 * Retrieves the user
	 * @return The user
	 */
	public String getUser()
	{
		return user;
	}

	/**
	 * Sets the user
	 * @param user The user to set
	 */
	public void setUser(String user)
	{
		this.user = user;
	}

	/**
	 * Retrieves the count
	 * @return The count
	 */
	public long getCount()
	{
		return count;
	}

	/**
	 * Sets the count
	 * @param user The count to set
	 */
	public void setCount(long count)
	{
		this.count = count;
	}
	
}
