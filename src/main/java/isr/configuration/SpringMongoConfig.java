package isr.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

/**
 * Configuration for MongoDB Repository
 */
@Configuration
public class SpringMongoConfig extends AbstractMongoConfiguration
{

    @Value("${spring.data.mongodb.host}")
    private String mongoHost;

    @Value("${spring.data.mongodb.port}")
    private String mongoPort;

    @Value("${spring.data.mongodb.database}")
    private String mongoDB;
 
    @Bean
    public Mongo mongo() throws Exception
    {
        return this.mongoClient();
    }

    @Override
    public MongoMappingContext mongoMappingContext()
        throws ClassNotFoundException
    {
        return super.mongoMappingContext();
    }

    @Override
    protected String getDatabaseName()
    {
        return mongoDB;
    }

	@Override
	public MongoClient mongoClient()
	{
		return new MongoClient(this.mongoHost + ":" + this.mongoPort);
	}
}