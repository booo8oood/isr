package isr.controller;

import java.util.List;
import java.util.Map;

import isr.exception.DateFormatException;

/**
 * LoginController
 */
public interface LoginController
{
	/**
	 * Retrieve the unique login dates
	 * @return login dates
	 */
	public Map<String, List<String>> getDates();

	/**
	 * Retrieve the users within a login date
	 * @param startDate Filters the login_time from startDate
	 * @param endDate Filters the login_time to endDate
	 * @return List of users with the login_time specified from start to end dates
	 * @throws DateFormatException The startDate or endDate is invalid
	 */
	public Map<String, List<String>> getUsers(String startDate, String endDate)
		throws DateFormatException;

	/**
	 * Retrieve the users with their corresponding login count
	 * @param startDate Filters the login_time from startDate
	 * @param endDate Filters the login_time to endDate
	 * @param attribute1 Filters the attribute1
	 * @param attribute2 Filters the attribute2
	 * @param attribute3 Filters the attribute3
	 * @param attribute4 Filters the attribute4
	 * @return List of users with the corresponding login count
	 * @throws DateFormatException The startDate or endDate is invalid
	 */
	public Map<String, Map<String, Long>> getLogins(String startDate, String endDate,
			String[] attribute1, String[] attribute2, String[] attribute3, String[] attribute4)
		throws DateFormatException;

	/**
	 * Populates the data
	 * @param size The size of the sample data
	 * @return message
	 */
	public String populate(String size);
}
