package isr.controller.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import isr.builder.LoginBuilder;
import isr.controller.LoginController;
import isr.entity.LoginCount;
import isr.exception.DateFormatException;
import isr.service.LoginService;
import isr.validator.LoginValidator;

/**
 * Controller for the Login Service
 */
@RestController
@RequestMapping("/test")
@Api(tags={"login"})
@SwaggerDefinition(tags={@Tag(name="login", description = "Login operations") })
public class LoginControllerImpl implements LoginController
{
	@Autowired
	private LoginService service;

	/**
	 * Retrieves the unique dates
	 * @return The list of unique dates
	 */
	@GetMapping(path="/dates", produces=MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Retrieves the unique dates", notes = "Retrieves the unique dates", tags={ "login" } )
	public Map<String, List<String>> getDates()
	{
	    Map<String, List<String>> output = new HashMap<>();
	    output.put("dates", service.getDates());
        return output;
	}

	/**
	 * Retrieves the unique users within the start and end parameters
	 * @param startDate The start date
	 * @param endDate The end date
	 * @return The list of unique users
	 * @throws DateFormatException The startDate or endDate is invalid
	 */
	@GetMapping("/users")
    @ApiOperation(value = "Retrieves the unique dates",
    	notes = "Retrieves the unique users within the specified start and end dates", tags={ "login" } )
	public Map<String, List<String>> getUsers(
			@RequestParam(value="start", required=false)
			@ApiParam(value="(Optional) The start date in YYYYMMDD format. Invalid formats will be ignored.")
			String startDate,
			@RequestParam(value="end", required=false)
			@ApiParam(value="(Optional) The end date in YYYYMMDD format. Invalid formats will be ignored.")
			String endDate)
		throws DateFormatException
	{
		LoginValidator.validateDate("start", startDate);
		LoginValidator.validateDate("end", endDate);

	    Map<String, List<String>> output = new HashMap<String, List<String>>();
	    output.put("users", service.getUsers(LoginBuilder.formatDate(startDate), LoginBuilder.formatDate(endDate)));
        return output;
	}

	/**
	 * Retrieves the list of users with the corresponding login counts
	 * @param startDate The start date
	 * @param endDate The end date
	 * @param attribute1 The attribute1 filter
	 * @param attribute2 The attribute2 filter
	 * @param attribute3 The attribute3 filter
	 * @param attribute4 The attribute4 filter
	 * @return The list of users with their corresponding login count
	 * @throws DateFormatException The startDate or endDate is invalid
	 */
	@GetMapping("/logins")
    @ApiOperation(value = "Retrieves the unique users with their corresponding login count within the start and end dates and attribute filters",
	notes = "Retrieves the unique users with their corresponding login count within the start and end dates and attribute filters", tags={ "login" } )
	public Map<String, Map<String, Long>> getLogins(
			@RequestParam(value="start", required=false)
			@ApiParam(value="(Optional) The start date in YYYYMMDD format. Invalid formats will be ignored.")
			String startDate,
			@RequestParam(value="end", required=false)
			@ApiParam(value="(Optional) The end date in YYYYMMDD format. Invalid formats will be ignored.")
			String endDate,
			@RequestParam(value="attribute1", required=false)
			@ApiParam(value="(Optional) attribute1 filters. Allows multiple values in the query parameters. e.g. ..?attribute1=X&attribute1=Y")
			String[] attribute1,
			@RequestParam(value="attribute2", required=false)
			@ApiParam(value="(Optional) attribute2 filters. Allows multiple values in the query parameters. e.g. ..?attribute2=X&attribute2=Y")
			String[] attribute2,
			@RequestParam(value="attribute3", required=false)
			@ApiParam(value="(Optional) attribute3 filters. Allows multiple values in the query parameters. e.g. ..?attribute3=X&attribute3=Y")
			String[] attribute3,
			@RequestParam(value="attribute4", required=false)
			@ApiParam(value="(Optional) attribute4 filters. Allows multiple values in the query parameters. e.g. ..?attribute4=X&attribute4=Y")
			String[] attribute4)
		throws DateFormatException
	{
		LoginValidator.validateDate("start", startDate);
		LoginValidator.validateDate("end", endDate);

		Map<String, Map<String, Long>> output = new HashMap<String, Map<String, Long>>();
		Map<String, Long> result = new HashMap<String, Long>();
	    List<LoginCount> loginCounts = service.getLogins(LoginBuilder.formatDate(startDate), LoginBuilder.formatDate(endDate),
	    		attribute1, attribute2, attribute3, attribute4);
	    for (LoginCount loginCount : loginCounts)
	    {
	    	result.put(loginCount.getUser(), loginCount.getCount());
	    }
	    output.put("logins", result);
		return output;
	}

	/**
	 * Populates the login table
	 */
	@GetMapping("/populate")
    @ApiOperation(value = "Populates the login table",
	notes = "Populates the login table with minimum of 100 and maximum of 100000."
			+ "The sample data range from 20180910 to 20180920, attribute1=login000001_attribute1, attribute2=login000001_attribute2...", tags={ "login" } )
	public String populate(
			@RequestParam(value="size", required=false)
			@ApiParam(value="(Optional) size of the sample data")
			String size)
	{
		if (size == null)
		{
			size = "0";
		}
		service.populateData(Integer.parseInt(size));

		return "Login sample data populated";
	}
}
