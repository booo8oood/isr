package isr.constants;

/**
 * Constants for the LoginService
 */
public class LoginConstants
{
	/** Maximum data size for populate **/
	public static final int MAX_DATA_SIZE = 100000;

	/** Default data size for populate **/
	public static final int DEFAULT_DATA_SIZE = 100;
}
