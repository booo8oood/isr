package isr.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import isr.entity.Login;
import isr.entity.LoginCount;
import isr.repository.LoginCustomRepository;

/**
 * Custom repository for Login service
 */
@Repository
public class LoginCustomRepositoryImpl implements LoginCustomRepository
{
    private final MongoTemplate mongoTemplate;

    /**
     * Mongo template
     * @param mongoTemplate
     */
    @Autowired
    public LoginCustomRepositoryImpl(MongoTemplate mongoTemplate)
    {
        this.mongoTemplate = mongoTemplate;
    }

    /**
     * Retrieve the unique users within the specified dates
     * @param startDate The start date
     * @param endDate The end date
     * @return The list of unique users within the specified dates
     */
	@Override
	public List<Login> getUsers(Date startDate, Date endDate)
	{
		List<AggregationOperation> operations = new ArrayList<AggregationOperation>();
		Criteria loginTimeCriteria = null;
		if (startDate != null)
		{
			loginTimeCriteria = Criteria.where("login_time").gte(startDate);		
		}

		if (endDate != null)
		{
			loginTimeCriteria.lte(endDate);
		}

		if (loginTimeCriteria != null)
		{
			operations.add(match(loginTimeCriteria));
		}

		operations.add(group("user"));
		operations.add(project("user").and("user").previousOperation());
		operations.add(sort(Sort.Direction.ASC, "user"));

		Aggregation agg = newAggregation(operations);
	
		//Convert the aggregation result into a List
		AggregationResults<Login> groupResults 
			= mongoTemplate.aggregate(agg, Login.class, Login.class);
		List<Login> result = groupResults.getMappedResults();
		return result;
	}

	/**
	 * Retrieve the unique users with the corresponding login counts
     * @param startDate The start date
     * @param endDate The end date
     * @param attribute1 The attribute1 filters
     * @param attribute2 The attribute2 filters
     * @param attribute3 The attribute3 filters
     * @param attribute4 The attribute4 filters
     * @return The unique users with the corresponding login counts
	 */
	public List<LoginCount> getLogins(Date startDate, Date endDate,
			 String[] attribute1,  String[] attribute2,  String[] attribute3,  String[] attribute4)
	{
		List<AggregationOperation> operations = new ArrayList<AggregationOperation>();
		Criteria loginTimeCriteria = null;
		if (startDate != null)
		{
			loginTimeCriteria = Criteria.where("login_time").gte(startDate);		
		}

		if (endDate != null)
		{
			loginTimeCriteria.lte(endDate);
		}

		if (loginTimeCriteria != null)
		{
			operations.add(match(loginTimeCriteria));
		}

		if (attribute1 != null)
		{
			operations.add(match(Criteria.where("attribute1").in(attribute1)));
		}

		if (attribute2 != null)
		{
			operations.add(match(Criteria.where("attribute2").in(attribute2)));
		}

		if (attribute3 != null)
		{
			operations.add(match(Criteria.where("attribute3").in(attribute3)));
		}

		if (attribute4 != null)
		{
			operations.add(match(Criteria.where("attribute4").in(attribute4)));
		}
		operations.add(group("user").count().as("count"));
		operations.add(project("count").and("user").previousOperation());
		operations.add(sort(Sort.Direction.DESC, "count"));

		Aggregation agg = newAggregation(operations);
	
		//Convert the aggregation result into a List
		AggregationResults<LoginCount> groupResults 
			= mongoTemplate.aggregate(agg, Login.class, LoginCount.class);
		List<LoginCount> result = groupResults.getMappedResults();
		return result;
	}
}
