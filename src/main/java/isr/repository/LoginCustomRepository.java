package isr.repository;

import java.util.Date;
import java.util.List;

import isr.entity.Login;
import isr.entity.LoginCount;

public interface LoginCustomRepository
{

	List<Login> getUsers(Date startDate, Date endDate);

	List<LoginCount> getLogins(Date startDate, Date endDate,
			String[] attribute1, String[] attribute2,  String[] attribute3, String[] attribute4);

}
