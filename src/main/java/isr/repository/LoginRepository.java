package isr.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import isr.entity.Login;

public interface LoginRepository extends MongoRepository<Login, String>
{
}