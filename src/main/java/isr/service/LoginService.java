package isr.service;

import java.util.Date;
import java.util.List;

import isr.entity.LoginCount;

/**
 * The interface of the LoginService
 */
public interface LoginService
{
	/**
	 * Retrieves a JSON array of all the unique dates (ignoring time) in the table
	 * @return The unique dates in the login table
	 */
	public List<String> getDates();

	/**
	 * Retrieves a JSON array of all the unique dates (ignoring time) in the table
	 * @param startDate The start date
	 * @param endDate The end date
	 * @return The unique dates in the table
	 */
	public List<String> getUsers(Date startDate, Date endDate);

	/**
	 * Retrieves a JSON array of all the unique dates (ignoring time) in the table
	 * @param startDate The start date
	 * @param endDate The end date
	 * @param attribute1 The attribute1 filters
	 * @param attribute2 The attribute2 filters
	 * @param attribute3 The attribute3 filters
	 * @param attribute4 The attribute4 filters
	 * @return The users with their corresponding login count
	 */
	public List<LoginCount> getLogins(Date startDate, Date endDate,
			String[] attribute1, String[] attribute2, String[] attribute3, String[] attribute4);

	/**
	 * Populates the Login table with the specified size (max is 100000 and default is 100)
	 * @param size The size of the data
	 */
	public void populateData(int size);
}
