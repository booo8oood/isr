package isr.service.impl;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import isr.builder.LoginBuilder;
import isr.constants.LoginConstants;
import isr.entity.Login;
import isr.entity.LoginCount;
import isr.repository.LoginCustomRepository;
import isr.repository.LoginRepository;
import isr.service.LoginService;

/**
 * The implementation of LoginService
 */
@Service
public class LoginServiceImpl implements LoginService
{
	/**
	 * LoginRepository
	 */
	@Autowired
	private LoginRepository repository;

	/**
	 * LoginCustomRepository
	 */
	@Autowired
	private LoginCustomRepository customRepository;

	/**
	 * Retrieves a JSON array of all the unique dates (ignoring time) in the table
	 * @return The unique dates in the login table
	 */
	public List<String> getDates()
	{
		List<String> loginTimeList = new ArrayList<String>();
		Sort sort = new Sort(Sort.Direction.ASC, "loginTime");
		for (Login login : repository.findAll(sort))
		{
			String loginTime = LoginBuilder.formatDate(login.getLoginTime());
			{
				if (!loginTimeList.contains(loginTime))
				{
					loginTimeList.add(loginTime);
				}
			}
		}
		return loginTimeList;
	}


	/**
	 * Retrieves a JSON array of all the unique dates (ignoring time) in the table
	 * @param startDate The start date
	 * @param endDate The end date
	 * @return The unique dates in the table
	 */
	public List<String> getUsers(Date startDate, Date endDate)
	{
		return LoginBuilder.buildUsers(customRepository.getUsers(startDate, endDate));
	}

	/**
	 * Retrieves a JSON array of all the unique dates (ignoring time) in the table
	 * @param startDate The start date
	 * @param endDate The end date
	 * @param attribute1 The attribute1 filters
	 * @param attribute2 The attribute2 filters
	 * @param attribute3 The attribute3 filters
	 * @param attribute4 The attribute4 filters
	 * @return The users with their corresponding login count
	 */
	public List<LoginCount> getLogins(Date startDate, Date endDate,
			String[] attribute1, String[] attribute2, String[] attribute3, String[] attribute4)
	{
		return customRepository.getLogins(startDate, endDate,
				attribute1, attribute2, attribute3, attribute4);
	}

	/**
	 * Populates the Login table with the specified size (max is 100000 and default is 100)
	 * For this sample data, The date is from 20180910 to 20180920
	 * @param size The size of the data
	 */
	public void populateData(int size)
	{
		repository.deleteAll();

		if (size <= 0)
		{
			size = LoginConstants.DEFAULT_DATA_SIZE;
		}
		else if (size > 100000)
		{
			size = LoginConstants.MAX_DATA_SIZE;
		}

		LocalDate startDate = LocalDate.of(2018, 9, 10);
		long start = startDate.toEpochDay();

		LocalDate endDate = LocalDate.of(2018, 9, 20);
		long end = endDate.toEpochDay();

		for (int cnt = 1; cnt <= size; cnt++)
		{
			long randomEpochDay = ThreadLocalRandom.current().longs(start, end).findAny().getAsLong();
			Date randomDate = Date.from(LocalDate.ofEpochDay(randomEpochDay).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			String count = String.format("%06d", cnt);
			Login login = new Login();
			login.setLoginTime(randomDate);
			login.setUser("login" + count);
			login.setAttribute1("login" + count + "_attribute1");
			login.setAttribute2("login" + count + "_attribute2");
			login.setAttribute3("login" + count + "_attribute3");
			login.setAttribute4("login" + count + "_attribute4");
			repository.save(login);
		}
	}

}
