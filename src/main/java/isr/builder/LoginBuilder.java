package isr.builder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import isr.entity.Login;

/**
 * Builder for the Login Service
 */
public final class LoginBuilder
{
	/**
	 * Formats the date to YYYYMMDD
	 * @param date The date to format
	 * @return Date in YYYYMMDD
	 */
	public static String formatDate(Date date)
	{
		DateFormat outputFormatter = new SimpleDateFormat("yyyyMMdd");
		String output = outputFormatter.format(date);
		return output;
	}

	/**
	 * Parse the date with format "yyyyMMdd"
	 * @param date in Date object
	 * @return date in Date object
	 */
	public static Date formatDate(String date)
	{
		DateFormat outputFormatter = new SimpleDateFormat("yyyyMMdd");
		Date output = null;
		if (date != null)
		{
			try
			{
				output = outputFormatter.parse(date);
			}
			catch (ParseException e)
			{
				
			}
		}

		return output;
	}

	/**
	 * Builds the list of users from the list of Login records
	 * @param records list of Login records
	 * @return list of users
	 */
	public static List<String> buildUsers(List<Login> records)
	{
		List<String> users = new ArrayList<String>();
		for (Login login : records)
		{
			users.add(login.getUser());
		}
		return users;
	}
}
